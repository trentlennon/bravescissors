var webdriver = require('selenium-webdriver'),
	_ = require('underscore'),
	fs = require("fs"),
	schedule = require('node-schedule'),
	util = require('util'),
	driver,
	index = require('./Tests/index.js'),
	resemble = require('node-resemble-js'),
	pathToBase ="./images/baseline/",
	pathToCurrent = "./images/current/",
	pathToDiff = "./images/diff/";

console.log("Beginning Tests.");
var job = schedule.scheduleJob('*/15 * * * * *', function(){
	//At some point going to add functionality to create multiple browsers.
	//Probably the same way we did index.js
	var tests = _.map(index, function(test) {
		return webdriver.promise.createFlow(function() {
			driver = new webdriver.Builder().forBrowser('firefox').build();
			test.test(driver);
			driver.quit();
		});
	});

	webdriver.promise.fullyResolved(tests).then(function() {
		console.log("all tests completed");
		ImageProcess(index);
	});

});

//Check if file exists. If it does.
function ImageProcess(index) {
	var tests = _.map(index, function(test) {
		console.log(JSON.stringify(test));
		var name = test.Interface + "-" + test.Page + ".png",
			baseline,
			current;

		try {
				baseline = fs.readFileSync(pathToBase + name);
				console.log("Base line found for " + name);

				current = fs.readFileSync(pathToCurrent + name);
				console.log("Read the current file.");

				var diff = resemble(baseline).compareTo(current).ignoreColors().onComplete(function(data) {
					data.getDiffImage().pack().pipe(fs.createWriteStream(pathToDiff + name)); //Packs it and takes the location.
				});
		}
		catch(err) {
			console.log(JSON.stringify(err));
			if(err.code === "ENOENT" && typeof baseline === "undefined") {
				//Move the current image into baseline.
				console.log("No baseline found. Moving current as new baseline.");
				MoveCurrentToBaseLine(name);

				//Remove the file from the current directory.
				console.log("Removing the current file with no baseline.");
				fs.unlinkSync(pathToCurrent + name);
			}
			else if(err.code === "ENOENT" && typeof current === "undefined") {
				console.log("No current file found for test. Make sure you're calling writeImage in your test.");
			}
			else {
				console.log(err);
			}
		}
	});
}

function MoveCurrentToBaseLine(name) {
	try {
		var imageFile = fs.readFileSync(pathToCurrent + name);
		fs.writeFileSync(pathToBase + name, imageFile);
	}
	catch(other) {
		console.log(other);
	}
}