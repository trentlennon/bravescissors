var fs = require('fs'),
	path = require('path'),
	base = require('./base.js');

module.exports = {

	Interface: "base",
	Page: "landing-page",

	//Driver is an implementation of Selenium web
	//driver and has all methods associated with such.
	test: function(driver) {
		driver.get('http://www.gmail.com');
		driver.manage().window().setSize(800,400);
		base.writeImageSync(driver, this.Interface, this.Page);
	}
}