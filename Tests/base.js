var fs = require('fs'),
	path = require('path');

module.exports = {
	//Driver is an implementation of Selenium web
	//driver and has all methods associated with such.
	writeImageSync: function(driver, inter, pageName) {
		driver.takeScreenshot().then(function(data){
			console.log("Taking Screenshot");
			var base64Data = data.replace(/^data:image\/png;base64,/,""),
				pathVar ="./images/current",
				fileName = inter + "-" + pageName + ".png";

			try {
				fs.writeFileSync(pathVar + "\\" + fileName, base64Data, 'base64');
			}
			catch(err) {
				console.log("Error: " + err);
			}
		});
	}
}

