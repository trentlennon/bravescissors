var fs = require('fs');

module.exports = {

	Interface: "Base",
	PageName: "OtherPage",

	//Driver is an implementation of Selenium web
	//driver and has all methods associated with such.
    test: function(driver) {
    	console.log("1");
    	driver.get('http://www.gmail.com');
    	driver.manage().window().setSize(800,400);
    	driver.takeScreenshot().then(function(data){
	        console.log("2");
	        var pageName = "OUT",
	            base64Data = data.replace(/^data:image\/png;base64,/,"");
		        fs.writeFile("test.png", base64Data, 'base64', function(err) {
	            driver.quit();

		        if(err) {
		            console.log(err);
		        }
        });
    });

    }
}